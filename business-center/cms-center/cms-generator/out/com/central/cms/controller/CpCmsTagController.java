package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsTag;
import com.central.cms.service.CpCmsTagService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsTagController extends BaseController {

    @Autowired
    private CpCmsTagService cpcmstagService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsTag cpcmstag = cpcmstagService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmstag);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmstagService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsTag cpcmstag){
        cpcmstagService.insertSelective(cpcmstag);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsTag cpcmstag){
        cpcmstagService.updateSelectiveById(cpcmstag);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsTag queryBean){
        PageInfo<CpCmsTag> page = cpcmstagService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsAdminSite;
import com.central.cms.service.CpCmsAdminSiteService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsAdminSiteController extends BaseController {

    @Autowired
    private CpCmsAdminSiteService cpcmsadminsiteService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsAdminSite cpcmsadminsite = cpcmsadminsiteService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsadminsite);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsadminsiteService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsAdminSite cpcmsadminsite){
        cpcmsadminsiteService.insertSelective(cpcmsadminsite);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsAdminSite cpcmsadminsite){
        cpcmsadminsiteService.updateSelectiveById(cpcmsadminsite);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsAdminSite queryBean){
        PageInfo<CpCmsAdminSite> page = cpcmsadminsiteService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

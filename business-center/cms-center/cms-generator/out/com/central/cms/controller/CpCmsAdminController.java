package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsAdmin;
import com.central.cms.service.CpCmsAdminService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsAdminController extends BaseController {

    @Autowired
    private CpCmsAdminService cpcmsadminService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsAdmin cpcmsadmin = cpcmsadminService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsadmin);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsadminService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsAdmin cpcmsadmin){
        cpcmsadminService.insertSelective(cpcmsadmin);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsAdmin cpcmsadmin){
        cpcmsadminService.updateSelectiveById(cpcmsadmin);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsAdmin queryBean){
        PageInfo<CpCmsAdmin> page = cpcmsadminService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsLinkage;
import com.central.cms.service.CpCmsLinkageService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsLinkageController extends BaseController {

    @Autowired
    private CpCmsLinkageService cpcmslinkageService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsLinkage cpcmslinkage = cpcmslinkageService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmslinkage);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmslinkageService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsLinkage cpcmslinkage){
        cpcmslinkageService.insertSelective(cpcmslinkage);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsLinkage cpcmslinkage){
        cpcmslinkageService.updateSelectiveById(cpcmslinkage);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsLinkage queryBean){
        PageInfo<CpCmsLinkage> page = cpcmslinkageService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

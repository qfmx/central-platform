package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsCategory;
import com.central.cms.service.CpCmsCategoryService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsCategoryController extends BaseController {

    @Autowired
    private CpCmsCategoryService cpcmscategoryService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsCategory cpcmscategory = cpcmscategoryService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmscategory);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmscategoryService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsCategory cpcmscategory){
        cpcmscategoryService.insertSelective(cpcmscategory);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsCategory cpcmscategory){
        cpcmscategoryService.updateSelectiveById(cpcmscategory);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsCategory queryBean){
        PageInfo<CpCmsCategory> page = cpcmscategoryService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

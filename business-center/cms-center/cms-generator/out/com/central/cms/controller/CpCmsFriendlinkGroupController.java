package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsFriendlinkGroup;
import com.central.cms.service.CpCmsFriendlinkGroupService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsFriendlinkGroupController extends BaseController {

    @Autowired
    private CpCmsFriendlinkGroupService cpcmsfriendlinkgroupService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsFriendlinkGroup cpcmsfriendlinkgroup = cpcmsfriendlinkgroupService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsfriendlinkgroup);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsfriendlinkgroupService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsFriendlinkGroup cpcmsfriendlinkgroup){
        cpcmsfriendlinkgroupService.insertSelective(cpcmsfriendlinkgroup);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsFriendlinkGroup cpcmsfriendlinkgroup){
        cpcmsfriendlinkgroupService.updateSelectiveById(cpcmsfriendlinkgroup);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsFriendlinkGroup queryBean){
        PageInfo<CpCmsFriendlinkGroup> page = cpcmsfriendlinkgroupService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}

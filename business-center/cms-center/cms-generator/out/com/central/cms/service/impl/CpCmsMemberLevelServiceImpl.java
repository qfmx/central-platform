package com.central.cms.service.impl;

import com.central.cms.service.CpCmsMemberLevelService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsMemberLevel;
import com.central.cms.mybatis.mapper.CpCmsMemberLevelMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsMemberLevelServiceImpl extends BaseServiceImpl<CpCmsMemberLevelMapper, CpCmsMemberLevel> implements CpCmsMemberLevelService {


}

package com.central.cms.service.impl;

import com.central.cms.service.CpCmsContentNewsService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsContentNews;
import com.central.cms.mybatis.mapper.CpCmsContentNewsMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsContentNewsServiceImpl extends BaseServiceImpl<CpCmsContentNewsMapper, CpCmsContentNews> implements CpCmsContentNewsService {


}

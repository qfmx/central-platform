package com.central.cms.service.impl;

import com.central.cms.service.CpCmsFriendlinkService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsFriendlink;
import com.central.cms.mybatis.mapper.CpCmsFriendlinkMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsFriendlinkServiceImpl extends BaseServiceImpl<CpCmsFriendlinkMapper, CpCmsFriendlink> implements CpCmsFriendlinkService {


}

package com.central.cms.service.impl;

import com.central.cms.service.CpCmsMemberOauthService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsMemberOauth;
import com.central.cms.mybatis.mapper.CpCmsMemberOauthMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsMemberOauthServiceImpl extends BaseServiceImpl<CpCmsMemberOauthMapper, CpCmsMemberOauth> implements CpCmsMemberOauthService {


}

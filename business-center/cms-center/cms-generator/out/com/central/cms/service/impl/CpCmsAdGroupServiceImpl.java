package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAdGroupService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAdGroup;
import com.central.cms.mybatis.mapper.CpCmsAdGroupMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAdGroupServiceImpl extends BaseServiceImpl<CpCmsAdGroupMapper, CpCmsAdGroup> implements CpCmsAdGroupService {


}

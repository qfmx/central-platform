package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAdService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAd;
import com.central.cms.mybatis.mapper.CpCmsAdMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAdServiceImpl extends BaseServiceImpl<CpCmsAdMapper, CpCmsAd> implements CpCmsAdService {


}

package com.central.eureka.client.config;

import java.util.Collections;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.central.eureka.client.filter.IpCilentFilter;
import com.central.eureka.client.filter.RequestAuthFilter;
import com.netflix.discovery.DiscoveryClient.DiscoveryClientOptionalArgs;

@Configuration
public class EurekaClintConfig {
	@Bean
	public DiscoveryClientOptionalArgs discoveryClientOptionalArgs() {
	    DiscoveryClientOptionalArgs discoveryClientOptionalArgs = new DiscoveryClientOptionalArgs();
	    discoveryClientOptionalArgs.setAdditionalFilters(Collections.singletonList(new IpCilentFilter()));
	    return discoveryClientOptionalArgs;
	}
	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
	    FilterRegistrationBean registration = new FilterRegistrationBean(new RequestAuthFilter());
	    registration.addUrlPatterns("/*");
	    return registration;
	}
	 
}
